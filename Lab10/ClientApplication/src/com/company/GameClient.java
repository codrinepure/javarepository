package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class GameClient {
    public static String serverAddress = "127.0.01";
    public static int PORT = 2000;

    public GameClient() {
        try {
            //creem un nou socket,adica un nou punct terminal al conexiunii, prin care se realizeaza conexiunea cu server ul specificand adresa ip si port
            Socket socket = new Socket(serverAddress,PORT);

            //creem un bufferedReader pentru a citi de la tastatura
            InputStreamReader reader = new InputStreamReader(System.in);
            BufferedReader inn = new BufferedReader(reader);

            //creem un bufferedReader pentru a citi din socket raspunsul de la server
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(socket.getInputStream())
            );

            //creem un printWriter pentru a trimite request uri catre server
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);

            String exit = "exit";

            while(true) {
                String request = inn.readLine();

                if(request.toString().equalsIgnoreCase(exit.toString())) {
                    break;
                }
                out.println(request);

                // asteptam raspunsul de la server
                String response = in.readLine();
                System.out.println(response);
            }


        }catch (UnknownHostException e) {
            System.err.println("No server listening..." + e);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


}
