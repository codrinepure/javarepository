package com.company;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class GameServer {
    public static final int PORT = 2000;

    public GameServer() throws IOException {
        ServerSocket serverSocket = null;
        try {
            //crearea si atasarea socket ului la adresa locala(port)
            serverSocket = new ServerSocket(PORT);
            System.out.println("Waiting at PORT " + PORT + "...");

            while(true) {

                //cu accept se blocheaza apelantul pana la sosirea unei cereri de conectare
                Socket socket = serverSocket.accept();

                //se utilizeaza socket ul returnat de accept si se creeaza un thread pentru fiecare client prin care se va realiza comunicarea cu serverul
                new ClientThread(socket,serverSocket).start();
            }
        }catch (IOException e) {
            System.err.println(e);
        } finally {
            serverSocket.close();
        }
    }
}
