package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class ClientThread extends Thread {
    private Socket socket = null;
    private ServerSocket serverSocket = null;

    public ClientThread(Socket socket, ServerSocket serverSocket) throws IOException {
        this.socket = socket;
        this.serverSocket = serverSocket;
    }

    public void run() {
        try {
            String stop = "stop";
            //preluam request ul de la client folosind un bufferedReader pentru a citi ceea ce trimite clientul prin socket
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            //creem un PrintWriter pentru a trimite raspunsul la client inapoi prin socket
            PrintWriter out = new PrintWriter(socket.getOutputStream());

            // citim e la tastatura si trimitem request la server
            while(true) {
                String request = in.readLine();

                //verificam daca request ul primit este "stop" si in caz afirmativ incheiem conexiunea cu clientul si oprim serverul,altfel instiintam clientul ca am primit request ul
                if (request.toString().equalsIgnoreCase(stop.toString())) {
                    String response = "Server stopped";
                    out.println(response);
                    out.flush();

                    try {
                        socket.close();
                        serverSocket.close();
                    } catch (IOException e) {
                        System.err.println((e));
                    }
                    break;
                } else {
                    String response = "Server received the request ... ";
                    out.println(response);
                    out.flush();
                    System.out.println(request);
                }
            }


        } catch (IOException e) {
            System.err.println(e);
        }
    }


}
