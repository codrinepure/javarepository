package com.company;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;
import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ConfigPanel extends JPanel {
    final MainFrame mainFrame;
    private JLabel labelShape;
    private JLabel labelColor;
    private JSpinner sides;
    private JComboBox colorCombo;
    private JComboBox shapeCombo;
    private static List<String> colorList = new ArrayList<>(Arrays.asList("red","yellow","blue"));
    private static List<String> shapesList = new ArrayList<>(Arrays.asList("square","circle"));

    public ConfigPanel(MainFrame mainframe)
    {
        this.mainFrame = mainframe;
        init();
    }

    private void init()
    {
        //setPreferredSize(new Dimension(500,500));
        labelShape = new JLabel(" Select shape:",JLabel.CENTER);
        labelShape.setFont( new Font(Font.SANS_SERIF,  Font.ITALIC, 15));
        labelShape.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED,Color.yellow,Color.RED));
        labelShape.setForeground(Color.blue);

        labelColor = new JLabel(" Select shape color:", JLabel.CENTER);
        labelColor.setFont( new Font(Font.SANS_SERIF,  Font.ITALIC, 15));
        labelColor.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED,Color.yellow,Color.red));
        labelColor.setForeground(Color.blue);

        sides = new JSpinner(new SpinnerNumberModel(0, 0, 20, 1));
        sides.setValue(4);


        colorCombo = new JComboBox(colorList.toArray());
        shapeCombo = new JComboBox(shapesList.toArray());


       add(labelShape);
       add(shapeCombo);
       add(labelColor);
       add(colorCombo);

    }

    public JComboBox getColorCombo() {
        return colorCombo;
    }

    public JComboBox getShapeCombo() {
        return shapeCombo;
    }
}
