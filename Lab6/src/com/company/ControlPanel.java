package com.company;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class ControlPanel extends JPanel {

    final MainFrame mainFrame;
    private JButton loadButton;
    private JButton saveButton;
    private JButton resetButton;
    private JButton exitButton;
    Graphics graphic;
    BufferedImage image;
    BufferedImage img;

    public ControlPanel(MainFrame mainframe)
    {
        this.mainFrame = mainframe;
        init();
    }

    private void init()
    {
        saveButton = new JButton("Save");
        loadButton = new JButton("Load");
        resetButton = new JButton("Reset");
        exitButton = new JButton("Exit");

        add(saveButton);
        add(loadButton);
        add(resetButton);
        add(exitButton);

        loadButton.addActionListener(this::load);
        saveButton.addActionListener(this::save);
        resetButton.addActionListener(this::reset);
        exitButton.addActionListener(this::exit);
    }

    @Override
    public void update(Graphics g) {
        super.update(g);
        g.drawImage(image,0,0,null);
    }

    @Override
    public void paintComponents(Graphics g) {
        super.paintComponent(g);
        g.drawImage(image,0,0,null);
    }

    private void load(ActionEvent e)
    {
        try {
            img = ImageIO.read(new File("java.png"));
            mainFrame.getCanvas().graphics.drawImage(img,0,0,null);

        } catch (IOException exception)
        {
            System.err.println(exception);

        }

    }
    private void save(ActionEvent e)
    {
        try {
            ImageIO.write(mainFrame.getCanvas().image,"PNG",new FileOutputStream("img.png"));
        } catch(IOException exception)
        {
            System.err.println(exception);
        }
    }
    private void reset(ActionEvent e)
    {
        mainFrame.getCanvas().reset(Color.green);
    }
    private void exit(ActionEvent e)
    {
        System.exit(0);
    }

}
