package com.company;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;

public class DrawingPanel extends JPanel {
    final MainFrame mainFrame;
    final static int width = 500;
    final static int height = 500;
    Graphics2D graphics;
    BufferedImage image;


    public DrawingPanel(MainFrame mainframe) {
        this.mainFrame = mainframe;
        createOffScreenImage(Color.black);
        init();

    }

    private void init() {
        setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED, Color.yellow, Color.RED));
        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                drawShape(e.getX(), e.getY());
                repaint();
            }
        });
    }

    private void createOffScreenImage(Paint color) {
        image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        graphics = image.createGraphics();
        graphics.setColor((Color) color);
        graphics.fillRect(0, 0, width, height);
    }

    private void drawShape(int x, int y) {

        var shape = this.mainFrame.getConfigPanel().getShapeCombo().getSelectedItem();
        var color = this.mainFrame.getConfigPanel().getColorCombo().getSelectedItem();
        var coloString = color.toString();

        graphics.setColor(
                (coloString.compareTo("red") == 0)
                        ? Color.red : (coloString.compareTo("yellow") == 0)
                        ? Color.yellow : Color.blue
        );

        if (shape.toString().compareTo("square") == 0) {
            graphics.fill(new Rectangle(x, y, 50, 50));
        } else {
            graphics.fillOval(x, y, 50, 50);
        }

    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(image, 0, 0, this);
    }

    public void reset(Paint color) {
        createOffScreenImage((Color) color);
    }


}
