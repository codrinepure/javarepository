package com.company;

import javax.swing.*;

import java.awt.*;

import static javax.swing.SwingConstants.*;

public class MainFrame extends JFrame {
    private ConfigPanel configPanel;
    private DrawingPanel canvas;
    private ControlPanel controlPanel;


    public MainFrame()
    {
        super("Drawing Shapes");
        init();
    }

    private void init ()
    {

        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setPreferredSize(new Dimension(500,500));
        this.setVisible(true);
        this.setLayout(new BorderLayout());

        configPanel = new ConfigPanel(this);
        canvas = new DrawingPanel(this);
        controlPanel = new ControlPanel(this);

        add(configPanel,BorderLayout.NORTH);
        add(canvas,BorderLayout.CENTER);
        add(controlPanel,BorderLayout.SOUTH);


        pack();
    }

    public ConfigPanel getConfigPanel() {
        return configPanel;
    }

    public DrawingPanel getCanvas() {
        return canvas;
    }

}
