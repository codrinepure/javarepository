package com.company;

import javax.print.Doc;
import java.io.*;

public class Main {

    public static void main(String[] args) throws IOException {


        var catalog = new Catalog();
        catalog.setSerializationFile("serializationFile.ser");

        var document1 = new Document();
        document1.setId("1");
        document1.setName("document1");
        document1.setPath("doc1.txt");
        document1.addTags("autor","Ion Creanga");
        document1.addTags("titlu","Amintiri din copilarie");
        document1.addTags("an","1881");
        document1.addTags("an publicatie","1892");

        var document2 = new Document();
        document2.setId("2");
        document2.setName("document2");
        document2.setPath("doc2.txt");
        document2.addTags("autor","Bram Stoker");
        document2.addTags("titlu","Dracula");
        document2.addTags("an","1897");
        document2.addTags("an publicatie","1897");
        System.out.println();

        catalog.addDocument(document1);
        catalog.addDocument(document2);

        var operation = new ExternalOperation();
        operation.setCatalog(catalog);
        operation.save();
        System.out.println();

        var newCatalog = operation.load(operation.getCatalog().getSerializationFile());
        newCatalog.displayCatalog();
        System.out.println();

        var d = new Document();
        d.setPath("test.txt");
        operation.view(d);

    }
}
