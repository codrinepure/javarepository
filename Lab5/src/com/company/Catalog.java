package com.company;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Catalog implements Serializable {
    private List<Document> documents;
    private String serializationFile;

    Catalog() {
        this.documents = new ArrayList<>();
    }

    public void addDocument(Object object)
    {
        if(object==null)
        {
            throw new IllegalArgumentException("The parameter shouldn't be null");
        }
        if(!(object instanceof Document))
        {
            throw new IllegalArgumentException("The parameter must be a Document object");
        }

        documents.add((Document)object);
    }

    public List<Document> getDocuments() {
        return documents;
    }

    public void setSerializationFile(String newSerializationFile) {
        if(newSerializationFile == null || newSerializationFile.trim().equals(""))
        {
            throw new IllegalArgumentException("The path or name file shouldn't empty");
        }

        this.serializationFile = newSerializationFile;
    }

    public String getSerializationFile() {
        return serializationFile;
    }

    public void displayCatalog()
    {
        for(var doc:documents)
        {
            doc.displayDocument();
            System.out.println();
        }
    }
}
