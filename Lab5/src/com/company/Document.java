package com.company;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Document implements Serializable {
    private String path;
    private String id;
    private String name;
    private Map<String, String> tags;
    private static List<String> ids = new ArrayList<>();

    Document() {
        this.tags = new HashMap<>();
    }

    public void setId(String newId) {
        if (newId == null) {
            throw new InvalidIdException("ID shouldn't be null");
        }
        if (existsId(newId) == true) {
            throw new InvalidIdException("ID must be unique");
        }

        this.id = newId;
        ids.add(newId);
    }

    private boolean existsId(String eId) {
        if (ids.size() == 0) return false;
        return ids.contains(eId);
    }

    public void setName(String newName) {
        if (newName == null || newName.trim().equals("")) {
            throw new InvalidNameException("Name shouldn't be empty");
        }

        this.name = newName;
    }

    public void addTags(String newKey, String newValue) {
        if (newKey == null || newKey.trim().equals("")) {
            throw new IllegalArgumentException("The key shouldn't be empty");
        }
        if (newValue == null || newValue.trim().equals("")) {
            throw new IllegalArgumentException("The key shouldn't be empty");
        }

        tags.put(newKey, newValue);
    }

    public void setPath(String newPath) {
        if (newPath == null || newPath.trim().equals("")) {
            throw new IllegalArgumentException("The Path shouldn't be empty");
        }

        this.path = newPath;
    }

    public String getPath() {
        return path;
    }

    public String getName() {
        return name;
    }

    public Map<String, String> getTags() {
        return tags;
    }

    public void displayDocument()
    {
        System.out.println(this.id);
        System.out.println(this.name);
        System.out.println(this.tags);
    }
}
