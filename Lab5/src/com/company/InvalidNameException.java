package com.company;

public class InvalidNameException extends RuntimeException {
    InvalidNameException(String message) {super(message);}
}
