package com.company;

import javax.print.Doc;
import java.awt.*;
import java.io.*;

public class ExternalOperation {
    private Catalog catalog;


    public void setCatalog(Object obj)
    {
        if(obj == null)
        {
            throw new IllegalArgumentException("The parameter should not be null");
        }
        if(!(obj instanceof Catalog))
        {
            throw new IllegalArgumentException("The parameter should be an instance of Catalog");
        }

        this.catalog = (Catalog) obj;
    }

    public void save()
    {
        try {
            var fileStream = new FileOutputStream(catalog.getSerializationFile());
            var objectStream = new ObjectOutputStream(fileStream);

            objectStream.writeObject(catalog);
            objectStream.close();
            fileStream.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Catalog load(String path)
    {
        Catalog newCatalog = null;
        try {
            var fileStream = new FileInputStream(path);
            var objectStream = new ObjectInputStream(fileStream);

            newCatalog = (Catalog) objectStream.readObject();

            objectStream.close();
            fileStream.close();

        } catch (IOException | ClassNotFoundException e)
        {
            e.printStackTrace();
        }

        return newCatalog;
    }

    public void view(Document doc) throws IOException {
        var desk = Desktop.getDesktop();
        File f = new File(doc.getPath());
        desk.open(f);
    }

    public Catalog getCatalog() {
        return catalog;
    }
}
