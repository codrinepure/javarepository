Partea obl⁮igatorie

1.Descriere implementare subpunct 1

Am creat clasele Catalog,Document si ExternalOperation care se va ocupa de operatiile cerute la subpunctu 2, adica: save, load si view.

Catalog va contine o lista de documente si un string serializationFile prin care se va seta fisierul prin care se va face ,,object serialization" de la metoda save ceruta la subpunctul 2.La fiecare metoda din clasele Catalog,respectiv Document am avut in vedere sa tratez si exceptii si pentru asta am creat propriile mele clase InvalidIdException si InvalidNameException si am mai folosit si IllegalArgumentException.

In clasa Document mi am creat o lista statica in care voi retine id-urile documentelor in timp ce el sunt adaugate la catalog si cu ea ma voi asigura  ca id urile sunt unice prin metoda existsId, astfel daca se ofera unui document un id dexa existent se va arunca exceptia "Id must be unique".De asemenea am declarat pentru fiecare document cate un map prin care sa retin tagurile pt acesta si anume informatii despre titlu,autor,year la fel cum sunt descrise in enuntul din laborator

2.Descriere implementare subpunct 2

In clasa ExternalOperation am utilizat metoda setCatalog cu exceptiile corespunzatoare prin care ma asigur ca daca ii dau ca argument un object si e null sau nu e instanta de Catalog atunci va arunca IllegalArgumentException

Apoi am implementat metoda save utilizand structura try-catch pentru lucrul cu filestream uri si pentru object serialization.Astfel, am facut serializarea utilizand fisierul de tip ser pe care l am dat ca argument la crearea FileOutputStream, iar apoi am folosit writeObject pentru a serializa catalogul meu cu toate documentele.

Print metoda load am realizat deserializarea folosind acelasi fisier de tip ser si mi am preluat catalogul stocat in fisier intr un obiect de tip catalog cu metoda read.Object

Pentru metoda view am utilizat clasa Desktop din java.awt si am apelat metoda getDesktop din aceasta clasa pentru a furniza o instanta a contextului actual al browserului.Apoi mi am creat un obiect de tip File dandu i ca argument path-ul, iar apoi am folosit metoda open tot din clasa desktop pentru a l deschide

In final in main mi am declarat doua documente, le-am umplut cu informatii, le am adaugat la catalog, am facut un obiect operation de tip ExternalOperation caruia i am transmis catalogul si ulterior am realizat toate operatiile cerute: save,load,view