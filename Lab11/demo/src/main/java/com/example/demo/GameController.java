package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/")
public class GameController {
    @Autowired
    GameRepository gameRepository;

    @GetMapping("/games")
    public List<Game> getGames(){
        return gameRepository.findAll();
    }
    @GetMapping("/games/{id}")
    public Game getGameById(@PathVariable int id){
        return gameRepository.findById(id).get();
    }

    @PostMapping(value = "/games", consumes="application/json")
    public ResponseEntity<String> createGame(@RequestBody Game game) {

        gameRepository.save(game);
        return new ResponseEntity<>("Game created successsfully", HttpStatus.CREATED);
    }
}
