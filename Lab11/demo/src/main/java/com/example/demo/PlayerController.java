package com.example.demo;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/")
public class PlayerController {

    @Autowired
    PlayerRepository playerRepository;

    @GetMapping("/players")
    public List<Player> getPlayers(){
        return playerRepository.findAll();
    }

    @PostMapping(value = "/players", consumes="application/json")
    public ResponseEntity<String> createPlayer(@RequestBody Player player) {

        playerRepository.save(player);
        return new ResponseEntity<>("Player created successsfully", HttpStatus.CREATED);
    }

    @PutMapping(value="/players/{id}", consumes="application/json")
    public ResponseEntity<String> updatePlayer(@PathVariable int id, @RequestBody String firstName) {
        Optional<Player> player = playerRepository.findById(id);
        if(player == null) {
            return new ResponseEntity<>("Player not found",HttpStatus.NOT_FOUND);
        }
        player.get().setFirstName(firstName);
        playerRepository.save(player.get());

        return new ResponseEntity<>("Player updated successfully",HttpStatus.OK);
    }

    @DeleteMapping("/players/{id}")
    public ResponseEntity<String> deletePlayer(@PathVariable int id) {
        Optional<Player> player = playerRepository.findById(id);

        if(player == null) {
            return new ResponseEntity<>("Player not found",HttpStatus.NOT_FOUND);
        }
        playerRepository.delete(player.get());
        return  new ResponseEntity<>("Player removed",HttpStatus.OK);
    }


}
