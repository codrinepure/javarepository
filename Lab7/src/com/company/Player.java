package com.company;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.locks.ReentrantLock;

import static java.lang.Thread.sleep;

public class Player implements Runnable {

    public List<Token> arrayTokens = new ArrayList<>();
    public int thinkingTime = 5000;
    private  String name;
    public Board board;

    public Player(String name,Board newBoard) {
        this.name = name;
        this.board = newBoard;
    }

    @Override
    public void run() {
        play();
    }

    /*
    prin functia play un jucator va incepe sa joace
    Thread.sleep() va fi folosit pentru a simula un timp de gandire al fiecarui jucator intre 2 si 5 secunde
    * */

    private void play() {
        while(board.leftPieces>0) {
            try {
                Thread.sleep((int) (2000 + ((5000) - 2000) * Math.random()));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            try {
                board.TakeToken(this);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /*
    * in functia Win vom verifica daca jucatorul curent are cumva o progresie aritmetica valida
    * vom lua toate perechile de token uri cu 2 for uri si vom afla toate ratiile posibile care ar putea sa existe in token urile extrase de acest jucator
    * apoi pentru fiecare ratie vom verifica daca array ul de token uri este o progresie aritmetica de ratia curenta
    * daca e valida si e de lungime egala cu arithmeticProgressionLength (k), jucatorul a castigat
    *
    * */

    public boolean Win() {
        Collections.sort(arrayTokens);
        var arrayRatio = new HashSet<Integer>();

        for(var i=0;i<arrayTokens.size();i++)
            for(var j=i+1;j<arrayTokens.size();j++) {
                var ratio = Math.abs(arrayTokens.get(i).getValue() -  arrayTokens.get(j).getValue());
                arrayRatio.add(ratio);
            }

        for(var ratio:arrayRatio) {
            boolean ok = true;
            int length = 1;
            for(var i=0;i<arrayTokens.size()-1;i++) {
                if(arrayTokens.get(i).isBlank() == true || arrayTokens.get(i+1).isBlank() == true) {
                    length++;
                    continue;
                }

                if(Math.abs(arrayTokens.get(i).getValue() - arrayTokens.get(i+1).getValue()) != ratio) {
                    ok=false;
                    break;
                }

                length++;
            }

            if(ok==true && length == Board.arithmeticProgressionLength) {
                System.out.println("Avem castigatorul " + this.name + " folosind ratia " + ratio);
                return true;
            }
        }

        return false;
    }

}
