package com.company;

import java.util.Random;

public class Token implements Comparable<Object> {
    private int value;
    private boolean blank;
    private boolean used;

    public Token(int value,boolean isBlank) {
        this.value = value;
        this.blank = isBlank;
        this.used = false;
    }


    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public boolean isBlank() {
        return blank;
    }

    public void setBlank(boolean blank) {
        this.blank = blank;
    }

    public boolean isUsed() {
        return used;
    }

    public void setUsed(boolean used) {
        this.used = used;
    }

    @Override
    public int compareTo(Object o) {
        int valueToken = ((Token)o).getValue();
        return this.value - valueToken;
    }
}
