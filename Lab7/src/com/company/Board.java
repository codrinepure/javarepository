package com.company;

import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

public class Board {
    public static int numberOfTokens = 30; //n
    public static int values = 100; //m
    public static int arithmeticProgressionLength = 2; //k
    public static boolean[] selectedToken = new boolean[values + 1];
    public static List<Token> arrayTokens = new ArrayList<>();
    public int leftPieces;
    public boolean playerChoose;


    public Board() {
        this.playerChoose = false;
        createBoard();
    }

    /*
        aici voi crea board-ul,adica voi lua toate token-urile si le voi adauga o valoare random intre 1 si m(values)
        cand am ales o valoare, o fac false pentru a nu mai putea fi aleasa iar,astfel pastrand token-urile cu valori distincte
        daca am ales cumva value+1, inseamna ca ace ltoken va fi blank
    * */

    private void createBoard() {

        for (var i = 0; i < values; i++)
            selectedToken[i] = false;

            leftPieces = numberOfTokens;

        for (var i = 0; i <= numberOfTokens; i++) {
            int randomValue = (int) (1 + ((values + 2) - 1) * Math.random());
            if (randomValue == values + 1) { //am ales blank
                var token = new Token(randomValue, true);
                arrayTokens.add(token);
            } else {
                selectedToken[randomValue] = true;
                var token = new Token(randomValue, false);
                arrayTokens.add(token);
            }
        }
    }


    /*
        un jucator va incerca sa aleaga un token
        daca playerChoose este true, adica un jucator alege, inseamna ca vom astepta
        vom pune jucatorul curent sa astepte si acesta va putea alege abia dupa ce playerChoose devine false
        ulterior ii vom anunta pe toti cei care asteapta ca pot alege prin notifyAll

        daca leftPieces ajunge 0, adica n au mai ramas tokeni in board sau daca un jucator a castigat jocul se termina
    * */

    public synchronized  void TakeToken(Player player) throws InterruptedException {
        try {
            if(playerChoose == true) {
                wait();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if(leftPieces==0 || player.Win() == true)  {
            System.out.println("Game is done");
            System.exit(0);
        }


        playerChoose = true;
        boolean ok=true;

        while(ok) {
            int randomValue = (int) (-1 + ((numberOfTokens) + 1) * Math.random());

            if(arrayTokens.get(randomValue).isUsed() == false) {
                arrayTokens.get(randomValue).setUsed(true);
                leftPieces-=1;
                playerChoose=false;
                player.arrayTokens.add(arrayTokens.get(randomValue));
                ok=false;
                notifyAll();
            }
        }
    }

}
