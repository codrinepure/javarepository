package com.company;

import java.util.ArrayList;
import java.util.List;

public class Game {

    public List<Player> arrayPlayers;

    public Game() {
        arrayPlayers = new ArrayList<>();
    }

    public void start() {
        for (var player : arrayPlayers) {
            new Thread(player).start();
        }
    }
}
