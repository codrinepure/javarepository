package com.company;

import java.util.Random;

public class Main {

    public static void main(String[] args) {

        var newBoard = new Board();
        var newGame = new Game();

        newGame.arrayPlayers.add(new Player("Codrin",newBoard));
        newGame.arrayPlayers.add(new Player("Mihai",newBoard));
        newGame.arrayPlayers.add(new Player("Valentin",newBoard));
        newGame.arrayPlayers.add(new Player("Cozma",newBoard));
        newGame.arrayPlayers.add(new Player("Diana",newBoard));

        newGame.start();
    }
}
