package Repo;

import Entity.AlbumsEntity;
import Entity.ArtistsEntity;

import javax.persistence.EntityManager;
import java.util.List;

public class AlbumRepository {

    private EntityManager entityManager;
    public AlbumRepository(EntityManager em) {
        this.entityManager = em;
    }

    public void create(AlbumsEntity albumsEntity) {
        entityManager.getTransaction().begin();
        entityManager.persist(albumsEntity);
        entityManager.getTransaction().commit();
    }

    public AlbumsEntity findById(int entityId) {
        var album = entityManager.find(AlbumsEntity.class,entityId);
        return album;
    }

    public List<AlbumsEntity> findByName(String name) {
        var albumsList = entityManager.createNamedQuery("Album.findByName")
                .setParameter("albumName",name)
                .getResultList();
        return albumsList;
    }

    public List<AlbumsEntity> findByArtist(ArtistsEntity artistsEntity) {
        var albumsListByArtist = entityManager.createNamedQuery("Album.findByArtist")
                .setParameter("artistId",artistsEntity.getId())
                .getResultList();

        return albumsListByArtist;
    }
}
