package Repo;


import Entity.ArtistsEntity;

import javax.persistence.EntityManager;
import java.util.List;

public class ArtistRepository {
    private EntityManager entityManager;
    public ArtistRepository(EntityManager em) {
        this.entityManager = em;
    }

    public void create(ArtistsEntity artistsEntity) {
        entityManager.getTransaction().begin();
        entityManager.persist(artistsEntity);
        entityManager.getTransaction().commit();
    }

    public ArtistsEntity findById(int entityId) {
        var artist = entityManager.find(ArtistsEntity.class,entityId);
        return artist;
    }

    public List<ArtistsEntity> findByName(String name) {
        var artistsList = entityManager.createNamedQuery("Artist.findByName")
                .setParameter("artistName",name)
                .getResultList();
        return artistsList;
    }
}
