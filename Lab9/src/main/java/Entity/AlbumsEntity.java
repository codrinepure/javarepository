package Entity;

import javax.persistence.*;


@NamedQueries({@NamedQuery(name="Album.findByName",
        query = "SELECT a FROM AlbumsEntity a WHERE a.name = :albumName"),
        @NamedQuery(name="Album.findByArtist",
                query = "SELECT a FROM AlbumsEntity a WHERE a.artistId = :artistId")
        })


@Entity
@Table(name = "albums", schema = "musicalbums", catalog = "")
public class AlbumsEntity {
    private int id;
    private String name;
    private Integer releaseYear;
    private Integer artistId;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 100)
    public String getName() {
        return name;
    }

    @Basic
    @Column(name = "artist_id", nullable = true)
    public Integer getArtistId() {
        return artistId;
    }

    public void setArtistId(Integer artistId) { this.artistId = artistId;}

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "release_year", nullable = true)
    public Integer getReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(Integer releaseYear) {
        this.releaseYear = releaseYear;
    }
}
