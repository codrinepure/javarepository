package Entity;

import javax.persistence.*;

@NamedQuery(name="Artist.findByName",
        query = "SELECT a FROM ArtistsEntity a WHERE a.name = :artistName")

@Entity
@Table(name = "artists", schema = "musicalbums", catalog = "")
public class ArtistsEntity {
    private int id;
    private String name;
    private String country;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 100)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "country", nullable = true, length = 100)
    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

}
