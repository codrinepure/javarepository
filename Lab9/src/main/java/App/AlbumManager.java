package App;

import Entity.AlbumsEntity;
import Entity.ArtistsEntity;
import Repo.AlbumRepository;
import Repo.ArtistRepository;
import Util.PersistenceUtil;

public class AlbumManager {
    public static void main(String[] args) {

        var pu = PersistenceUtil.getInstance();
        var entityManager = pu.getEntityManagerFactory().createEntityManager();

        var artistRepository = new ArtistRepository(entityManager); //  instantiez repository ul pentru artist

        var artist = new ArtistsEntity(); // creez un artist si il salvez in baza de date
        artist.setName("artist");
        artist.setCountry("romania");
        artistRepository.create(artist);

        System.out.println(artistRepository.findById(1).getName()); //aduc din baza de date artistul cu id ul respectiv

        for (var eachArtist:  artistRepository.findByName("andu")) { //afisez numele pentru fiecare artisst gasit dupa numele respectiv
            System.out.println(eachArtist.getName());
        }

        var albumRepository = new AlbumRepository(entityManager);

        var album = new AlbumsEntity(); // creez un album si il salvez in baza de date
        album.setName("album");
        album.setReleaseYear(2020);
        album.setArtistId(1);
        albumRepository.create(album);

        System.out.println(albumRepository.findById(1).getName()); //aduc din baza de date albumul cu id ul respectiv


        for (var eachAlbum:  albumRepository.findByName("diverse")) { //afisez anul pentru fiecare album gasit dupa numele respectiv
            System.out.println(eachAlbum.getReleaseYear());
        }

        var artistForAlbum = artistRepository.findById(1); // pregatesc artistul pentru metoda findByArtist din albumRepository

        for(var eachAlbum: albumRepository.findByArtist(artistForAlbum)) { // afisez numele fiecarui album care are artistul respectiv
            System.out.println(eachAlbum.getName());
        }

        entityManager.close();

    }
}
