package Util;


import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class PersistenceUtil {
    private static PersistenceUtil instance;
    private String persistenceUnitName = "MusicAlbumsPU";
    private EntityManagerFactory entityManagerFactory;

    private PersistenceUtil() {
       this.entityManagerFactory =  Persistence.createEntityManagerFactory(persistenceUnitName);
    }

    public EntityManagerFactory getEntityManagerFactory() {
        return this.entityManagerFactory;
    }

    public static PersistenceUtil getInstance() {
        if(instance ==null) {
            instance = new PersistenceUtil();
        }
        return instance;
    }
}
