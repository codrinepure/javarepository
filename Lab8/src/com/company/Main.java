package com.company;

import java.sql.*;

public class Main {

    public static void main(String[] args) throws SQLException {


	    var connection=Database.getInstance();
	    var artistController = new ArtistController(connection);
	    var albumController = new AlbumController(connection);

	    artistController.create("delia","romania");
	    artistController.findByName("delia");

        System.out.println();

	    albumController.create("diverse",4,2018);
	    albumController.findByArtist(10);
    }
}
