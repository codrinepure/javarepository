package com.company;

import com.company.Database;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

public class ArtistController {

    private Database connection;
    private Statement statement;

    public ArtistController(Database conn) throws SQLException {
        this.connection = conn;
        createStatement();
    }

    private void createStatement() throws SQLException {
        this.statement = connection.createStatement();
    }

    public void create(String name, String country) throws SQLException {
        this.statement.executeUpdate("INSERT INTO artists (name,country) VALUES ('" + name + "','" + country + "')");
    }

    public void findByName(String name) throws SQLException {
         ResultSet result = this.statement.executeQuery("SELECT * FROM artists WHERE name='" + name+ "'");

         if(!result.isBeforeFirst()) {
             System.out.println("Nu a fost gasit niciun artist");
             return;
         }
         else {
             ResultSetMetaData resultMetaData= result.getMetaData();

             int columnCount = resultMetaData.getColumnCount();

             for(var i =1;i<=columnCount;i++)
             {
                 System.out.print(resultMetaData.getColumnName(i)+ " ");
             }

             System.out.println();

             while(result.next())
             {
                 for(int i=1;i<=columnCount;i++)
                 {
                     System.out.print(result.getString(i) + " ");
                 }
                 System.out.println();
             }
         }

    }
}
