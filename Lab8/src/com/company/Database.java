package com.company;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class Database {
    private static Database instance;
    private Connection connection;
    private String url = "jdbc:mysql://localhost/musicalbums";
    private String username = "dba";
    private String password = "sql";

    private Database() throws SQLException {
        try {
            this.connection = DriverManager.getConnection(url, username, password);
        } catch (SQLException e) {
            System.err.println("Cannot connect to DB: " + e);
        }
    }

    public Connection getConnection() {
        return this.connection;
    }

    public Statement createStatement () throws SQLException {
        return this.connection.createStatement();
    }

    public static Database getInstance() throws SQLException {
        if (instance == null) {
            instance = new Database();
        } else if (instance.getConnection().isClosed()) {
            instance = new Database();
        }

        return instance;
    }

}
