package com.company;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

public class AlbumController {
    private Database connection;
    private Statement statement;

    public AlbumController(Database conn) throws SQLException {
        this.connection = conn;
        createStatement();
    }

    private void createStatement() throws SQLException {
        this.statement = connection.createStatement();
    }

    public void create(String name, int artistId, int releaseYear) throws SQLException {
        this.statement.executeUpdate("INSERT INTO albums (name,artist_id,release_year) VALUES ('" + name + "','" + artistId + "','" + releaseYear +"')");
    }

    public void findByArtist(int idArtist) throws SQLException {
        ResultSet result = this.statement.executeQuery("SELECT * FROM albums WHERE artist_id=" + idArtist);
        if(!result.isBeforeFirst()) {
            System.out.println("Nu a fost gasit niciun album");
            return;
        }
        else {
            ResultSetMetaData resultMetaData= result.getMetaData();

            int columnCount = resultMetaData.getColumnCount();

            for(var i =1;i<=columnCount;i++)
            {
                System.out.print(resultMetaData.getColumnName(i)+ " ");
            }

            System.out.println();

            while(result.next())
            {
                for(int i=1;i<=columnCount;i++)
                {
                    System.out.print(result.getString(i) + " ");
                }
                System.out.println();
            }
        }
    }
}
