package com.company;

import javax.swing.*;
import java.awt.*;

public class MainFrame extends JFrame {
    private ControlPanel controlPanel;
    private DesignPanel designPanel;

    public MainFrame() {
        super("Swing Designer");
        this.init();
    }

    private void init() {
        this.setPreferredSize(new Dimension(700, 700));
        this.setVisible(true);
        this.setLayout(new BorderLayout());
        this.designPanel = new DesignPanel(this);
        this.controlPanel = new ControlPanel(this);
        this.add(this.designPanel, "Center");
        this.add(this.controlPanel, "North");
        this.pack();
    }

    public ControlPanel getControlPanel() {
        return controlPanel;
    }

    public DesignPanel getDesignPanel() {
        return designPanel;
    }

    public static void main(String[] args) {
    var mainFrame = new MainFrame();
    }
}
