package com.company;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ControlPanel extends JPanel {
    final MainFrame mainFrame;
    private JLabel labelComponent;
    private JButton createButton;
    private JComboBox componentOptionsCombo;
    private static List<String> componentsList = new ArrayList(Arrays.asList("button", "label"));

    public ControlPanel(MainFrame mainFrame) {
        this.mainFrame = mainFrame;
        this.init();
    }

    private void init() {
        this.labelComponent = new JLabel(" Select component:", 0);
        this.labelComponent.setFont(new Font("SansSerif", 2, 15));
        this.labelComponent.setForeground(Color.blue);
        this.componentOptionsCombo = new JComboBox(componentsList.toArray());
        this.add(this.labelComponent);
        this.add(this.componentOptionsCombo);
        this.createButton = new JButton("Create component");
        this.add(this.createButton);
        this.createButton.addActionListener(this::create);
    }

    private void create(ActionEvent e) {
        try {
            String command = e.getActionCommand();
            if (command.equals("Create component")) {
                var designPanel = this.mainFrame.getDesignPanel();

                Object component = this.componentOptionsCombo.getSelectedItem();
                String componentString = component.toString();

                if (componentString.compareTo("button") == 0) {
                    designPanel.createButton();
                } else if (componentString.compareTo("label") == 0) {
                    designPanel.createLabel();
                }
            }
        } catch (ClassNotFoundException |  InstantiationException | IllegalAccessException ex) {
            System.err.println(ex);
        }
    }

    public JComboBox getComponentOptionsCombo() {
        return componentOptionsCombo;
    }
}
