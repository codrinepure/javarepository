package com.company;

import javax.swing.*;
import java.awt.*;
public  class DesignPanel extends JPanel {
    final MainFrame mainFrame;
    public DesignPanel(MainFrame mainFrame) {
        this.mainFrame = mainFrame;
        this.init();
    }

    private void init() {
       this.setBackground(Color.red);
    }

    public void createButton() throws ClassNotFoundException, InstantiationException, IllegalAccessException {
        Insets insets = this.getInsets();
        Class cls = Class.forName("javax.swing.JButton");
        var newButton = (JButton) cls.newInstance();
        newButton.setText("button");
        this.add(newButton);
        Dimension size = newButton.getPreferredSize();
        newButton.setBounds(100 + insets.left, 50 + insets.top,size.width,size.height);
    }
    public void createLabel() throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        Insets insets = this.getInsets();
        Class cls = Class.forName("javax.swing.JLabel");
        var newLabel = (JLabel) cls.newInstance();
        newLabel.setText("Label component");
        newLabel.setFont(new Font("SansSerif", 2, 15));
        newLabel.setForeground(Color.blue);
        this.add(newLabel);
        Dimension size = newLabel.getPreferredSize();
        newLabel.setBounds(300 + insets.left, 50 + insets.top,size.width,size.height);
    }
}
